﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace good
{
    public class MovieImages
    {
        public ImageSource ImagesFrom { get; set; }
    }

    public class ImagesAndURLs
    {
        public string URLs { get; set; }

        public MovieImages Source AllImages { get; set; }
    }

    public partial class MainPage : ContentPage
    {
        public static int NumberOfColums = 4;
        public static int NumberOfImages = 20;
        public static int HeightOfImages = 200;
        public static int WidthOfImages = 400;

        public static List<ImagesAndURLs> MovieImageList = ImagesLinks(NumberOfImages, HeightOfImages, WidthOfImages);

        public MainPage()
        {
            InitializeComponet();

            DisplayImages(MovieImageList);
        }
        
        public static List<ImagesAndURLs> ImagesLinks(int CountImages, int Width, int Height)
    {
        List<ImagesAndURLs> AllInfo = new List<ImagesAndURLs>();

        Random Round = new Random();


        for (var i = 0; i < CountImages; i++) //dont really understand this part 
        {
            var r1 = Round.Next(1, 10);

            string WhereIsImage = $"http://www.imdb.com/title/tt1375666/mediaviewer/rm3426651392";

            var URL = $"{WhereIsImage}{r1}";

            var URLLocation = new UriImageSource { Uri = new Uri(URL) };
            URLLocation.CachingEnabled = false;

            AllInfo.Add(new ImagesAndURLs { AllURLs = URL, AllImages = new MovieImages { ImagesFrom = URLLocation } });
           
        }

        foreach (var a in AllInfo)
        {
            System.Diagnostics.Debug.WriteLine(a.AllURLs);
        }

        return AllInfo;
    }

    void DisplayImages(List<ImagesAndURLs> AllInfo)
    {
        Grid grid = new Grid
        {
            VerticalOptions = LayoutOptions.FillAndExpand,
            RowSpacing = 0,
            ColumnSpacing = 0,

            ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
            },
        

        RowDefinitions = new RowDefinitionCollection
        {
            new RowDefinition { Height = new GridLength(1, GridUnitType.Star)}
        }
    };

        for (var i = 0; i < AllInfo.Count; i++)
        {
            var Image = new Image
            {
                Source = AllInfo[i].AllImages.ImagesFrom,
                Aspect = Aspect.Fill,
            };

            var Indicator = new ActivityIndicator
            {
                IsRunning = true,
            };

            var TouchImage = new TapGestureRecognizer();
            TouchImage.Tapped += Tapping;
            Image.GestureRecognizers.Add(TouchImage);

            var b = Convert.ToInt32(i % NumberOfColums);

            var c = Convert.ToInt32(i / NumberOfColums);

            Grid.SetColumn(Image, b);
            Grid.SetRow(Image, c);

            Grid.SetColumn(Indicator, b);
            Grid.SetRow(Indicator, c);

            grid.Children.Add
            (
                Indicator
            );

            grid.Children.Add
            (
                Image
            );
           
        }

        this.Contect = grid;

        System.Diagnostics.Debug.WriteLine($"count children - {grid.Children.Count}");
    }

    async void Tapping(object sender, EventArgs d)
    {
        var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * NumberOfColums;

        var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);

        await Navigation.PushAsync(new DetailPage(MovieImageList[a + b].AllURLs));
    }
}
