﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MoviesApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            var ImageButton = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter { Property = Button.BorderRadiusProperty, Value = 0},
                    new Setter { Property = Image.SourceProperty} //CONTINUE HERE, NEEDS MORE WORK
                }
            };

            var ImageGrid = new Grid { RowSpacing = 1, ColumnSpacing = 1 };
            {

                ImageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                ImageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                ImageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });

                ImageGrid.Children.Add(new Image { Source = "http://lorempixel.com/50/50/sports/1/" }, 1, 1);
                ImageGrid.Children.Add(new Image { Source = "http://lorempixel.com/50/50/sports/1/" }, 1, 1);
                ImageGrid.Children.Add(new Image { Source = "http://lorempixel.com/50/50/sports/1/" }, 1, 1);

                Content = ImageGrid;
            
            }

        }
    }
}
